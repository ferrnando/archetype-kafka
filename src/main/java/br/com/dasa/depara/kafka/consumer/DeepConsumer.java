
package br.com.dasa.depara.kafka.consumer;

import br.com.dasa.depara.dtos.json.aninhado.ExamReportAninhado;
import br.com.dasa.depara.kafka.producer.ConverterFhir;
import br.com.dasa.depara.kafka.producer.AlvinProducer;
import br.com.dasa.depara.config.kafka.consumer.KafkaConsumerConfig;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@AllArgsConstructor
public class DeepConsumer {

    private AlvinProducer AlvinProducer;
    private ConverterFhir converterFhir;

    @KafkaListener(topics = KafkaConsumerConfig.TOPIC_NAME_CONSUMER,
            groupId = KafkaConsumerConfig.GROUP_ID ,
            containerFactory = KafkaConsumerConfig.CONTAINER_NAME
    )
    public void consumerListener(@Payload ExamReportAninhado message) {
        // @Header(KafkaHeaders.RECEIVED_PARTITION_ID) int partition
        log.info("Received Message: " + message + " partition-id: ");
        AlvinProducer.sendMessage(converterFhir.execute(message));
    }
}

