package br.com.dasa.depara.kafka.producer;

import br.com.dasa.depara.dtos.FhirDTO;
import br.com.dasa.depara.dtos.json.aninhado.ExamReportAninhado;
import org.springframework.stereotype.Component;


@Component
public class ConverterFhir {

    static final int MILLIS_SECUNDS = 3000;

    public FhirDTO execute(ExamReportAninhado message) {

        try {

            System.out.println("Convertendo para Fhir = " + message);
            Thread.sleep(MILLIS_SECUNDS);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return new FhirDTO("Fhier","Luiz","Teste sangue","Normal");
    }
}
