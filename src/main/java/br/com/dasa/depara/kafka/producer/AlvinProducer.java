
package br.com.dasa.depara.kafka.producer;

import br.com.dasa.depara.dtos.FhirDTO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

@Slf4j
@AllArgsConstructor
@Service
public class AlvinProducer {

    private KafkaTemplate<String, FhirDTO> kafkaTemplate;

    public void sendMessage(FhirDTO message) {

        Assert.notNull(message, "'message' must not be null.");
        System.out.println("message = Enviando mensagem para outro topico" );
/*      final ListenableFuture<SendResult<String, FhierDTO>> future = kafkaTemplate.send(kafkaProducerConfig.TOPIC_NAME,Producer.class.getName(), message);

        future.addCallback(new ListenableFutureCallback<SendResult<String, FhierDTO>>() {

            @Override
            public void onSuccess(SendResult<String, FhierDTO> result) {
                log.info("Sent message=[" + message + "] with offset=[" + result.getRecordMetadata().offset() + "]");
            }
            @Override
            public void onFailure(Throwable ex) {
                log.info("Unable to send message=["+ message + "] due to : " + ex.getMessage());
            }
        });*/
    }
}
