package br.com.dasa.depara.config.kafka.consumer;

import br.com.dasa.depara.config.kafka.KafkaConfig;
import br.com.dasa.depara.dtos.json.aninhado.ExamReportAninhado;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import java.util.HashMap;
import java.util.Map;

@EnableKafka
@Configuration
public class KafkaConsumerConfig {

    public static final String TOPIC_NAME_CONSUMER = "teste-producer";
    public static final String GROUP_ID = "testers";
    public static final String CONTAINER_NAME="kafkaListenerContainerFactory";

    @Bean
    public ConsumerFactory<String, ExamReportAninhado> consumerFactory() {

        Map<String, Object> props = new HashMap<>();
        props.put( ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, KafkaConfig.BOOTSTRAP_ADDRESS);
        props.put( ConsumerConfig.GROUP_ID_CONFIG,GROUP_ID);
        return new DefaultKafkaConsumerFactory<>(props,
                new StringDeserializer(),
                new JsonDeserializer<>(ExamReportAninhado.class));
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, ExamReportAninhado> kafkaListenerContainerFactory() {

        ConcurrentKafkaListenerContainerFactory<String, ExamReportAninhado> factory =
                new ConcurrentKafkaListenerContainerFactory<>();

        factory.setConsumerFactory(consumerFactory());

        return factory;
    }

}