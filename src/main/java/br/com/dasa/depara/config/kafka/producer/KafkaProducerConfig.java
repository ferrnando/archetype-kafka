package br.com.dasa.depara.config.kafka.producer;

import br.com.dasa.depara.config.kafka.KafkaConfig;
import br.com.dasa.depara.dtos.FhirDTO;
import br.com.dasa.depara.dtos.json.aninhado.ExamReportAninhado;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;

import java.util.HashMap;
import java.util.Map;


@Configuration
public class KafkaProducerConfig {

    public static final String TOPIC_NAME = "teste-producer";

    @Bean
    public KafkaTemplate<String, FhirDTO> kafkaTemplate() {
        return new KafkaTemplate<>(producerFactory());
    }
    @Bean
    public ProducerFactory<String, FhirDTO> producerFactory() {
        Map<String, Object> configProps = new HashMap<>();

        configProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, KafkaConfig.BOOTSTRAP_ADDRESS);

        return new DefaultKafkaProducerFactory<String, FhirDTO>(configProps,
                new StringSerializer(),
                new JsonSerializer<FhirDTO>());
    }


    @Bean
    public KafkaTemplate<String, ExamReportAninhado> kafkaTemplateExam() {return new KafkaTemplate<>(producerFactoryExam());}
    @Bean
    public ProducerFactory<String, ExamReportAninhado> producerFactoryExam() {
        Map<String, Object> configProps = new HashMap<>();

        configProps.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG,KafkaConfig.BOOTSTRAP_ADDRESS);

        return new DefaultKafkaProducerFactory<String, ExamReportAninhado>(configProps,
               new StringSerializer(),
               new JsonSerializer<ExamReportAninhado>());
    }


}