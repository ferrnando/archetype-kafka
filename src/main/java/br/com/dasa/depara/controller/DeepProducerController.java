package br.com.dasa.depara.controller;

import br.com.dasa.depara.dtos.json.aninhado.ExamReportAninhado;
import com.fasterxml.jackson.core.JsonProcessingException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.util.concurrent.ListenableFuture;
import org.springframework.util.concurrent.ListenableFutureCallback;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.FileNotFoundException;
import java.io.IOException;
import br.com.dasa.depara.config.kafka.producer.KafkaProducerConfig;

@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping("/deep-producer")
class DeepProducerController {

    private final KafkaTemplate<String, ExamReportAninhado> kafkaTemplate;


    @GetMapping("/message/{count}/{intervalInSecunds}")
    public ResponseEntity sendMessage(@PathVariable Integer count ,
                            @PathVariable Integer intervalInSecunds) throws InterruptedException {

        messageHandle(count, intervalInSecunds);

        return ResponseEntity.ok().body("Messages sent Successfully");
    }

    private void messageHandle(Integer count, Integer intervalInSecunds) throws InterruptedException {
        int interator= 1;
        while ( interator <= count){

            this.sendMessage();

            Thread.sleep(intervalInSecunds * 1000);
            interator ++;
        }
    }

    private void sendMessage(){
        try {
            final var examReport = ExamReportAninhado.build();

            final ListenableFuture<SendResult<String, ExamReportAninhado>> future = kafkaTemplate.send(
                    KafkaProducerConfig.TOPIC_NAME,
                    DeepProducerController.class.getName(),
                    examReport);

            future.addCallback(new ListenableFutureCallback<>() {
                @Override
                public void onSuccess(SendResult<String, ExamReportAninhado> result) {
                    SuccessMessageCallBack(result);
                }
                @Override
                public void onFailure(Throwable ex) {
                    ErrorMessageCallBack(ex);
                }
            });
        } catch (JsonProcessingException | FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void ErrorMessageCallBack(Throwable ex){
        log.info("Fail to send message error=["+ ex.getCause().getLocalizedMessage()+"] " + ex.getMessage());

    }
    private void SuccessMessageCallBack(SendResult<String, ExamReportAninhado> result){
        log.info("Sent message=[" + result.getProducerRecord().value()+ "]  with offset=[" + result.getRecordMetadata().offset() + "]");
    }
}
