package br.com.dasa.depara.dtos.json.separado.items;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Material {
    private String codigoIntegracao;
    private String nome;
}
