package br.com.dasa.depara.dtos.json.separado.items;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
@Setter
@Getter
public class Exame {
    private int sequencia;
    private String status;
    private String codigoOrigem;
    private String nome;
    private String nomeLaudo;
    private List<Solicitante> solicitantes;
    private ResponsavelTecnico responsavelTecnico;
    private List<Resultado> resultados;
    private List<String> tags;
}
