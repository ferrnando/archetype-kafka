package br.com.dasa.depara.dtos.json.separado;

import br.com.dasa.depara.dtos.json.separado.items.Paciente;
import br.com.dasa.depara.dtos.json.separado.items.Requisicao;
import br.com.dasa.depara.dtos.json.separado.items.Solicitante;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;
@Setter
@Getter
@ToString
public class ExamRerportMotion {

    private String sistemaOrigem;
    private String marcaOrigem;
    private Paciente paciente;
    private List<Solicitante> solicitantes;
    private List<Requisicao> requisicoes;
    private List<Object> tags;

}
