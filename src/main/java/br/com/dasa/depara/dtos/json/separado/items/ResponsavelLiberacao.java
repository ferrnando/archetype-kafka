package br.com.dasa.depara.dtos.json.separado.items;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ResponsavelLiberacao {
    private String nome;
    private String imagemAssinatura;
    private String complemento;
}
