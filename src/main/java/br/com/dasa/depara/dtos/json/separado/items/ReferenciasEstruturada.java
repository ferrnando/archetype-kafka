package br.com.dasa.depara.dtos.json.separado.items;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ReferenciasEstruturada {
    private String inclusiveMin;
    private String inclusiveMax;
    private String valorMin;
    private String valorMax;
    private String valorMaxFormatado;
    private String valorMinFormatado;
    private String valor;
}
