package br.com.dasa.depara.dtos.json.aninhado;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import lombok.Getter;
import lombok.Setter;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Getter @Setter
public class ExamReportAninhado {
    private String sistemaOrigem;
    private String marcaOrigem;
    private Paciente paciente;
    private List<Solicitante> solicitantes;
    private List<Requisicao> requisicoes;
    private List<Object> tags;
    private static final String PATH_JSON = "src/main/resources/jsonDeep.txt";

    public static ExamReportAninhado build() throws IOException {
        final var objectMapper = new ObjectMapper();

        objectMapper.registerModule(new JavaTimeModule());

        final Path path = Paths.get(PATH_JSON);

        final var toParse = Files.readString(path);

        return objectMapper.readValue(toParse, ExamReportAninhado.class);
    }
    @Override
    public String toString() {
        return "ExamReportAninhado{" +
                "sistemaOrigem='" + sistemaOrigem + '\'' +
                ", marcaOrigem='" + marcaOrigem + '\'' +
                ", paciente=" + paciente +
                ", solicitantes=" + solicitantes +
                ", requisicoes=" + requisicoes +
                ", tags=" + tags +
                '}';
    }

    static class Filho {
        public String chave;
        public String valor;
        public List<Filho> filhos;

    }

    static class Hospital {
        public String codigoIntegracao;
        public String nome;
    }

    static class InformacaoHospitalar {
        public String codigoHospitalar;
        public String internado;
        public String codigoIntegracaoTipoArea;
        public String siglaTipoArea;
        public String nomeTipoArea;
        public String nomeAreaHospital;
        public Hospital hospital;
    }

    static class Amostra {
        public String sequencia;
        public String codigo;
        public String descricao;
        public LocalDateTime dataColeta;
        public String dataUltimaRefeicao;
        public String tipoUltimaRefeicao;
        public LocalDateTime dataAdmissao;
        public Material material;
    }

    static class Comentario {
        public String valor;
        public String tipo;
    }

    static class EnderecoCompleto {
        public String logradouro;
        public String numero;
        public String cep;
    }

    static class Exame {
        public int sequencia;
        public String status;
        public String codigoOrigem;
        public String nome;
        public String nomeLaudo;
        public List<Solicitante> solicitantes;
        public ResponsavelTecnico responsavelTecnico;
        public List<Resultado> resultados;
        public List<String> tags;
    }

    static class LocaisExecucao {
        public String nome;
        public List<String> produtos;
    }

    static class Material {
        public String codigoIntegracao;
        public String nome;
    }

    static class Metadado {
        public String chave;
        public String valor;
        public List<Filho> filhos;
    }

    static class Metodo {
        public String codigo;
        public String descricao;
    }

    static class Paciente {
        public String codigoOrigem;
        public String codigoDestino;
        public String nome;
        public String sexo;
        public LocalDate dataNascimento;
        public String enderecoFormatado;
        public EnderecoCompleto enderecoCompleto;
    }

    static class ReferenciasEstruturada {
        public String inclusiveMin;
        public String inclusiveMax;
        public String valorMin;
        public String valorMax;
        public String valorMaxFormatado;
        public String valorMinFormatado;
        public String valor;
    }

    static class Requisicao {
        public String codigoOrigem;
        public String senha;
        public String codigoDestino;
        public LocalDateTime dataRequisicao;
        public List<Exame> exames;
        public List<Amostra> amostras;
        public List<LocaisExecucao> locaisExecucao;
        public Metadado metadado;
        public String responsavelTecnico;
        public String nomeUnidadeResponsavel;
        public String codigoIntegracaoUnidadeResponsavel;
        public InformacaoHospitalar informacaoHospitalar;
    }

    static class ResponsavelDigitacao {
        public String nome;
        public String complemento;
    }

    static class ResponsavelLiberacao {
        public String nome;
        public String imagemAssinatura;
        public String complemento;
    }

    static class ResponsavelTecnico {
        public String nome;
        public String complemento;
    }

    static class Resultado {
        public int sequencia;
        public String codigo;
        public String codigoAlternativo;
        public String descricao;
        public String nomeLaudo;
        public List<SubResultado> subResultados;
        public List<String> tags;
        public List<Comentario> comentarios;
        public String comentarioLaudoAntes;
        public String comentarioLaudoDepois;
        public boolean hasPanico;
        public boolean hasParcial;
        public String status;
        public String tipo;
        public Metodo metodo;
        public String valor;
        public String valorNaoFormatado;
        public String unidadeMedida;
        public String valorReferencia;
        public List<ReferenciasEstruturada> referenciasEstruturadas;
        public LocalDateTime dataLiberacao;
        public LocalDateTime dataDigitacao;
        public ResponsavelLiberacao responsavelLiberacao;
        public ResponsavelDigitacao responsavelDigitacao;
        public Amostra amostra;
        public UsoMedicacao usoMedicacao;
    }

    static class Solicitante {
        public long sequencia;
        public String nome;
        public String tipoConselho;
        public String ufConselho;
        public String numeroConselho;
    }

    static class SubResultado {
        public int sequencia;
        public String tipo;
        public String codigo;
        public String codigoAlternativo;
        public String descricao;
        public String nomeLaudo;
        public Metodo metodo;
        public String valor;
        public String valorNaoFormatado;
        public String unidadeMedida;
        public String valorReferencia;
        public List<ReferenciasEstruturada> referenciasEstruturadas;
        public List<Object> subResultados;
        public LocalDateTime dataLiberacao;
        public LocalDateTime dataDigitacao;
        public ResponsavelLiberacao responsavelLiberacao;
        public ResponsavelDigitacao responsavelDigitacao;
        public List<String> tags;
        public Amostra amostra;
        public String comentarioLaudoAntes;
        public String comentarioLaudoDepois;
        public boolean hasPanico;
        public boolean hasParcial;
        public String status;
    }

    static class UsoMedicacao {
        public String medicacao;
    }
}
