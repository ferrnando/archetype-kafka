package br.com.dasa.depara.dtos.json.separado.items;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class EnderecoCompleto {
    private String logradouro;
    private String numero;
    private String cep;
}
