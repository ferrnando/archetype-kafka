package br.com.dasa.depara.dtos.json.separado.items;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class InformacaoHospitalar {
    private String codigoHospitalar;
    private String internado;
    private String codigoIntegracaoTipoArea;
    private String siglaTipoArea;
    private String nomeTipoArea;
    private String nomeAreaHospital;
    private Hospital hospital;
}
