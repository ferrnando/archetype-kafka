package br.com.dasa.depara.dtos.json.separado.items;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class ResponsavelTecnico {
    private String nome;
    private String complemento;
}
