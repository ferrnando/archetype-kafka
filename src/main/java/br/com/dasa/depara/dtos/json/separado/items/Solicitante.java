package br.com.dasa.depara.dtos.json.separado.items;

import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class Solicitante {
    private long sequencia;
    private String nome;
    private String tipoConselho;
    private String ufConselho;
    private String numeroConselho;
}
