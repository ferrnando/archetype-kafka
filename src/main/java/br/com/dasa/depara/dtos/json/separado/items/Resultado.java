package br.com.dasa.depara.dtos.json.separado.items;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;
@Setter
@Getter
public class Resultado {
    private int sequencia;
    private String codigo;
    private String codigoAlternativo;
    private String descricao;
    private String nomeLaudo;
    private List<SubResultado> subResultados;
    private List<String> tags;
    private List<Comentario> comentarios;
    private String comentarioLaudoAntes;
    private String comentarioLaudoDepois;
    private boolean hasPanico;
    private boolean hasParcial;
    private String status;
    private String tipo;
    private Metodo metodo;
    private String valor;
    private String valorNaoFormatado;
    private String unidadeMedida;
    private String valorReferencia;
    private List<ReferenciasEstruturada> referenciasEstruturadas;
    private LocalDateTime dataLiberacao;
    private LocalDateTime dataDigitacao;
    private ResponsavelLiberacao responsavelLiberacao;
    private ResponsavelDigitacao responsavelDigitacao;
    private Amostra amostra;
    private UsoMedicacao usoMedicacao;
}
