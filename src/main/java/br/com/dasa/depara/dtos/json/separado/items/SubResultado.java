package br.com.dasa.depara.dtos.json.separado.items;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;
@Setter
@Getter
public class SubResultado {
    private int sequencia;
    private String tipo;
    private String codigo;
    private String codigoAlternativo;
    private String descricao;
    private String nomeLaudo;
    private Metodo metodo;
    private String valor;
    private String valorNaoFormatado;
    private String unidadeMedida;
    private String valorReferencia;
    private List<ReferenciasEstruturada> referenciasEstruturadas;
    private List<Object> subResultados;
    private LocalDateTime dataLiberacao;
    private LocalDateTime dataDigitacao;
    private ResponsavelLiberacao responsavelLiberacao;
    private ResponsavelDigitacao responsavelDigitacao;
    private List<String> tags;
    private Amostra amostra;
    private String comentarioLaudoAntes;
    private String comentarioLaudoDepois;
    private boolean hasPanico;
    private boolean hasParcial;
    private String status;
}
