package br.com.dasa.depara.dtos.json.separado.items;


import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
@Setter
@Getter
public class Paciente {
    private String codigoOrigem;
    private String codigoDestino;
    private String nome;
    private String sexo;
    private LocalDateTime dataNascimento;
    private String enderecoFormatado;
    private EnderecoCompleto enderecoCompleto;
}
