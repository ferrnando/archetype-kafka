package br.com.dasa.depara.dtos.json.separado.items;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;
@Setter
@Getter
public class Requisicao {
    private String codigoOrigem;
    private String senha;
    private String codigoDestino;
    private LocalDateTime dataRequisicao;
    private List<Exame> exames;
    private List<Amostra> amostras;
    private List<LocaisExecucao> locaisExecucao;
    private Metadado metadado;
    private String responsavelTecnico;
    private String nomeUnidadeResponsavel;
    private String codigoIntegracaoUnidadeResponsavel;
    private InformacaoHospitalar informacaoHospitalar;
}
