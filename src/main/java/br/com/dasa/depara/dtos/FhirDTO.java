package br.com.dasa.depara.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter
@AllArgsConstructor
public class FhirDTO {
    private String name;
    private String paciente;
    private String observations;
    private String contact;

}
