package br.com.dasa.depara.dtos.json.separado.items;

import lombok.Getter;
import lombok.Setter;

@Setter @Getter
public class Comentario {
    private String valor;
    private String tipo;
}
