package br.com.dasa.depara.dtos.json.separado.items;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
@Setter
@Getter
public class LocaisExecucao {
    private String nome;
    private List<String> produtos;
}
