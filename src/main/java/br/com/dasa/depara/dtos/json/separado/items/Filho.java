package br.com.dasa.depara.dtos.json.separado.items;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
@Setter
@Getter
public class Filho {
    private String chave;
    private String valor;
    private List<Filho> filhos;
}
