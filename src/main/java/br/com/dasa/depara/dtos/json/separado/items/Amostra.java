package br.com.dasa.depara.dtos.json.separado.items;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter @Setter
public class Amostra {
    private String sequencia;
    private String codigo;
    private String descricao;
    private LocalDateTime dataColeta;
    private String dataUltimaRefeicao;
    private String tipoUltimaRefeicao;
    private LocalDateTime dataAdmissao;
    private Material material;
}
