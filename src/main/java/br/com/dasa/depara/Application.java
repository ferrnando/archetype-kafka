package br.com.dasa.depara;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.EnableKafka;


@EnableKafka
@SpringBootApplication
public class Application {

	public static void main(String[] args)  {
		SpringApplication.run(Application.class, args);
	//	new SpringApplicationBuilder(Application.class).web(WebApplicationType.NONE).run();
	}
}
