#!/usr/bin/env bash

echo '################### BUILD- CLEAN PACKAGE ######################'
cd ..
mvn clean package -DskipTests

echo '################### CLEANING CONTAINERS ######################'
cd docker
docker-compose down
docker system prune -f

echo '################### STARTING MICROSERVICE  ######################'
docker-compose up -d --build api

echo '################### CLEANING OLD IMAGE AND OLD VOLUME  ######################'
docker image prune -f
docker volume prune -f

echo '################### RUN KAFKA SCRIPT ######################'
creation-topic.bat teste-producer

echo '################### FINISHING - HAVE FUN ! REGARDS ! ######################'