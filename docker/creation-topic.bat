echo off

echo '################### CREATE TOPIC ######################'
call docker container exec -d kafka_container_1 kafka-topics --create --partitions 1 --replication-factor 1 --topic %1 --bootstrap-server localhost:9092
echo 'TOPIC' %1 'CREATED'