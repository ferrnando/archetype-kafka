echo off

echo '################### BUILD- CLEAN PACKAGE ######################'
cd ..
call mvn clean package -DskipTests

echo '################### CLEANING CONTAINERS ######################'
cd .\docker\
call docker-compose down
call docker system prune -f

echo '################### STARTING MICROSERVICE  ######################'
call docker-compose up -d --build api

echo '################### CLEANING OLD IMAGE AND OLD VOLUME  ######################'
call docker image prune -f
call docker volume prune -f

echo '################### RUN KAFKA SCRIPT ######################'
call creation-topic.bat teste-producer

echo '################### FINISHING - HAVE FUN ! REGARDS ! ######################'